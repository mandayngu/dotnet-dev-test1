﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

 public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        //xac dinh so lon thu hai 
        if (x.Length < 2)
        {
            return -1;
        }
        int firstNum = FindTheBigNumber(x);
        int secondNum = x[0];
        int vitri = 0;
        int count = 0;
       
        for(int i  = 0; i< x.Length; i++)
        {
            if (x[i] < firstNum && x[i] > secondNum)
            {
                secondNum= x[i];
            }
            if(secondNum == x[i])
            {
                count++;
            }
        }
        if (count > 0)
        {
            for (int i = 0; i < x.Length; i++)
            {

                if (secondNum == x[i])
                {
                    return i ;
                }
            }
        }
        for (int i = 0; i < x.Length; i++)
        {

            if (secondNum == x[i])
            {
                vitri = i;
            }
        }

        return vitri  ;

    }

    public static int FindTheBigNumber(int[] x)
    {
        //để giải quuyết vấn đề nhieu so lon nhat thì nếu đếm số lớn nhất lớn hơn 2 thì trả về vị trí thứ nhất nó gặp
        int destinationBigOne = 0;
        int countBigOne = 0;
        int result = x[0];
        for(int i = 0; i< x.Length; i++)
        {
            if(result < x[i])
            {
                result = x[i];
            }
            if(result == x[i])
            {
                countBigOne ++;
            }

        }
        if(countBigOne > 0)
        {
            for(int i = 0 ; i< x.Length ; i++) {
          
            if(result == x[i])
                {
                    return i+1;
                }
            }
        }

        return result;
    }
}