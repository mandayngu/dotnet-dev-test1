﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

   public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần

        int length = x.Length;

        for (int i = 0; i < length - 1; i++)
        {   //length-1 b/c i+1 will put you OOB

            if (x[i] < x[i + 1])
            {//For descending order...skips to next index if false

                int temp = x[i];
                x[i] = x[i + 1];
                x[i + 1] = temp;

                i = -1; //sort from lowest out of order index
            }
        }

        return x ;
    }

}